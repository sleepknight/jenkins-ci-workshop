# jenkins-conversion-workshop

- Jenkins conversion lab is broken into three sections:

    -   Pipeline conversion from Jenkins to GitLab
    -   Application containerization based on the maven app
    -   GitLab pipelines and security integration

The workshop is based on the [project](https://gitlab.com/gitlab-com/customer-success/professional-services-group/global-practice-development/consulting/jenkins-conversion.git). 

